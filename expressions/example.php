<?php
/*$a=10;
$b=10;
//$a=$a+1; //$a++ or na+=1 .post increment
echo $a++; //10
echo"<br>";
echo $a;
echo "<br>";
echo ++$a;
echo "<br>";
echo $a;

echo "<br>";
echo $a;
*/
function double($i)
{
    return $i*2;
}

$b = $a = 5;


/* assign the value five into the variable $a and $b */
$c = $a++;
echo $c; //5 /* post-increment, assign original value of $a
                      // (5) to $c */

echo $a;
echo "<br>";
$e = $d = ++$b;

echo "<br>";/* pre-increment, assign the incremented value of
                       $b (6) to $d and $e */
echo $e;

/* at this point, both $d and $e are equal to 6 */

$f = double($d++);  // assign twice the value of $d before
            echo $f;
echo "<br>";
echo "<br>";
//12  //      the increment, 2*6 = 12 to $f
$g = double(++$f); //25 // assign twice the value of $e after
               echo $g;
echo "<br>";
// the increment, 2*7 = 14 to $g
$h = $g += 10;      // first, $g is incremented by 10 and ends with the
echo "<br>";
echo "<br>";
echo $h;